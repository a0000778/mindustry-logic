staticVars:
	set sampleNum 0
	set negativeCount 0
sharedVars:
	set linkN 0
chainBegin:
	set selfSwitch gate1
	set prevSwitch gate2
	set nextSwitch gate3
	jump isChainHead strictEqual prevSwitch null
	sensor allowEnable prevSwitch @enabled
	jump disableBegin strictEqual allowEnable 0
isChainHead:
	jump isChainTail strictEqual nextSwitch null
	sensor nextEnable nextSwitch @enabled
	jump enableBegin strictEqual nextEnable 1
isChainTail:
	sensor selfEnable selfSwitch @enabled
	jump highPowerCheck strictEqual selfEnable 1
lowPowerCheck:
	sensor powerStore reactor1 @powerNetStored
	sensor powerCap reactor1 @powerNetCapacity
	op div powerStoreRatio powerStore powerCap
	op max powerStoreRatio powerStoreRatio 0
	jump preEnable lessThan powerStoreRatio 0.6
	jump disableBegin always 0 0
highPowerCheck:
	sensor powerIn reactor1 @powerNetIn
	sensor powerOut reactor1 @powerNetOut
	op sub powerSum powerIn powerOut
	op add sampleNum sampleNum 1
	jump positivePower greaterThan powerSum 3600
	op add negativeCount negativeCount 1
positivePower:
	jump waitSample lessThan sampleNum 30
	jump disableBegin lessThan negativeCount 10
	set sampleNum 0
	set negativeCount 0
waitSample:
	jump enableBegin always 0 0
disableBegin:
	control enabled selfSwitch 0 0 0 0
disableLoopBegin:
	getlink link linkN
	sensor linkType link @type
	jump disableNotGen notEqual linkType @thorium-reactor
	control enabled link 0 0 0 0
disableNotGen:
	op add linkN linkN 1
	jump disableLoopBegin lessThan linkN @links
	jump sharedVars always 0 0
preEnable:
	set sampleNum 0
	set negativeCount 0
enableBegin:
	control enabled selfSwitch 1 0 0 0
checkGenLoopBegin:
	getlink link linkN
	sensor linkType link @type
	jump checkNext notEqual linkType @thorium-reactor
	sensor cf link @cryofluid
	jump lowCf lessThanEq cf 10
	jump highCf greaterThanEq cf 20
	jump checkNext always 0 0
highCf:
	control enabled link 1 0 0 0
	jump checkNext always 0 0
lowCf:
	control enabled link 0 0 0 0
checkNext:
	op add linkN linkN 1
	jump checkGenLoopBegin lessThan linkN @links
	jump sharedVars always 0 0
