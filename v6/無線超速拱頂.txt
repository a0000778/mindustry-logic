#基於 單位運輸從核心.txt 改出
!waitInit:
	jump !waitInit lessThan @links 1
	op mul USE_UNIT_FLAG @thisx 10000
	op add USE_UNIT_FLAG USE_UNIT_FLAG @thisy
	getlink TARGET 0
	sensor TARGET_X TARGET @x
	sensor TARGET_Y TARGET @y
	set HIGH_ITEM_NUM 250
	set LOW_ITEM_NUM 150
	jump !checkReq always 0 0
!idle:
!unitReset:
	ucontrol flag 0 0 0 0 0
	ubind null
!checkReq:
	sensor $targetSilicon TARGET @silicon
	sensor $targetPhase TARGET @phase-fabric
	jump !reqSilicon lessThan $targetSilicon $targetPhase
!reqPhase:
	jump !idle greaterThan $targetPhase LOW_ITEM_NUM
	set $useItemType @phase-fabric
	jump !takeItem always 0 0
!reqSilicon:
	jump !idle greaterThan $targetSilicon LOW_ITEM_NUM
	set $useItemType @silicon
	jump !takeItem always 0 0
!unitBind:
	ubind @flare
	jump !unitBind strictEqual @unit null
	sensor $unitDead @unit @dead
	jump !unitBind notEqual $unitDead 0
	sensor $unitFlag @unit @flag
	jump !unitBind notEqual $unitFlag 0
	sensor $unitCtrld @unit @controlled
	jump !unitBind notEqual $unitCtrld 0
	ucontrol flag USE_UNIT_FLAG 0 0 0 0
	sensor $unitItemMax @unit @itemCapacity
!takeItem:
	sensor $unitDead @unit @dead
	jump !unitBind notEqual $unitDead 0
	sensor $unitFlag @unit @flag
	jump !unitReset notEqual $unitFlag USE_UNIT_FLAG
	sensor $unitCtrler @unit @controller
	jump !unitReset notEqual $unitCtrler @this
	ulocate building core false @copper $coreX $coreY $coreExists $core
	ucontrol boost 1 0 0 0 0
	ucontrol approach $coreX $coreY 3 0 0
	ucontrol itemDrop $core $unitItemMax 0 0 0
	ucontrol itemTake $core $useItemType $unitItemMax 0 0
	sensor $unitItemCount @unit $useItemType
	jump !takeItem lessThan $unitItemCount $unitItemMax
!dropItem:
	sensor $unitDead @unit @dead
	jump !unitBind notEqual $unitDead 0
	sensor $unitFlag @unit @flag
	jump !unitReset notEqual $unitFlag USE_UNIT_FLAG
	sensor $unitCtrler @unit @controller
	jump !unitReset notEqual $unitCtrler @this
	ucontrol boost 1 0 0 0 0
	ucontrol approach TARGET_X TARGET_Y 3 0 0
	ucontrol itemDrop TARGET $unitItemMax 0 0 0
	sensor $targetItem TARGET $useItemType
	jump !checkReq greaterThan $targetItem HIGH_ITEM_NUM
	sensor $unitItemCount @unit $useItemType
	jump !takeItem lessThan $unitItemCount 10
	jump !dropItem always 0 0
